package uebung4_1;

public class UeberweisungDurchfuehren{
	
	private double betrag;
	private Konto quelle;
	private Konto ziel;
	
	public UeberweisungDurchfuehren(double betrag, Konto quelle, Konto ziel){
		this.betrag = betrag;
		this.quelle = quelle;
		this.ziel = ziel;
	}
	
	public void ueberweisen(double betrag, Konto quelle, Konto ziel){
		double quelleKontostand = quelle.getKontostand();
		double zielKontostand = ziel.getKontostand();
		assert(quelleKontostand + zielKontostand == quelle.getKontostand() + ziel.getKontostand());	
		quelle.abheben(betrag);
		ziel.einzahlen(betrag);
		assert(quelleKontostand - betrag == quelle.getKontostand() && zielKontostand + betrag == ziel.getKontostand() && quelleKontostand + zielKontostand == quelle.getKontostand() + ziel.getKontostand());
	}
}
