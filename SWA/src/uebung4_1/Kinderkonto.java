package uebung4_1;

public class Kinderkonto extends Konto {
	
	public Kinderkonto(double kontostand, double maxAbhebeBetrag, double minKontostand, double betrag){
		super(kontostand, maxAbhebeBetrag, minKontostand);
		this.minKontostand = 0; 
	}
	
	@Override
	public void abheben(double betrag){
		double alterKontostand = kontostand;
		assert(minKontostand >= 0);
		this.kontostand -= betrag;
		assert(alterKontostand == getKontostand() + betrag);
	}
	
	@Override
	public void einzahlen(double betrag){
		double alterKontostand = kontostand;
		assert(betrag <= maxAbhebeBetrag);
		this.kontostand += betrag;
		assert(alterKontostand == getKontostand() - betrag);
	}
	
}
