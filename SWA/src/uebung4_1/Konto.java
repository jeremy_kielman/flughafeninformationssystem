package uebung4_1;

public class Konto {
	
	protected double kontostand;
	protected double maxAbhebeBetrag;
	protected double minKontostand;
	
	public Konto(double kontostand, double maxAbhebeBetrag, double minKontostand){
		this.kontostand = kontostand;
		this.maxAbhebeBetrag = maxAbhebeBetrag;
		this.minKontostand = minKontostand;
	}
	
	public void abheben(double betrag){
		double alterKontostand = kontostand;
		assert(betrag <= maxAbhebeBetrag);
		this.kontostand -= betrag;
		assert(alterKontostand == getKontostand() + betrag);
	}
	
	public void einzahlen(double betrag){
		double alterKontostand = kontostand;
		assert(betrag <= maxAbhebeBetrag);
		this.kontostand += betrag;
		assert(alterKontostand == getKontostand() - betrag);
	}
	
	public double getKontostand(){
		return kontostand;
	}
}
