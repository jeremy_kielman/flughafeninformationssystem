package uebung4_2;

// Datei: Dreieck.java
import java.awt.Graphics;
import java.awt.Polygon;

final public class Dreieck extends GrafischesElement
{
   int seitenlaenge;
   Polygon dreieck;

   public Dreieck(int x, int y, int seitenlaenge) 
   {
      super(x, y);
      this.seitenlaenge = seitenlaenge;
   }

   public Polygon getDreieck(int seitenlaenge) 
   {
      // Ein Dreieck wird als Polygon betrachtet:
      // Zuerst werden die Koordinaten der Eckpunkte berechnet und 
      // dann ein Polygon erzeugt. Das Polygon wird als Ergebnis 
      // zurueckgegeben.
      int xArr[]={getX()-(seitenlaenge/2),
                  getX()+(seitenlaenge/2),getX()};
      int yArr[]={getY()+(seitenlaenge/2),getY()+(seitenlaenge/2),
                  getY()-(seitenlaenge/2)};
      // Neues Polygon zurueckgeben
      return new Polygon(xArr, yArr, 3);
   }

   public void zeichne(Graphics g) 
   {
      dreieck = getDreieck(seitenlaenge);
      g.fillPolygon(dreieck);
   }

   public boolean liegtPunktImElement (int x, int y) 
   {
      return dreieck.contains(x,y);
   } 
}
