package uebung4_2;

// Datei: Viereck.java
import java.awt.Graphics;
import java.awt.Rectangle;

public class Viereck extends GrafischesElement 
{ 
   int laenge, hoehe;
   Rectangle viereck;
   
   public Viereck(int x, int y, int laenge, int hoehe) 
   {
      super(x, y);
      this.laenge = laenge;
      this.hoehe = hoehe;
   }
   
   // Implementierte zeichne()-Methode der Basisklasse
   public void zeichne(Graphics g) 
   {
      // Viereck erstellen
      viereck = new Rectangle(this.x - (int) (0.5 * laenge),
                      this.y - (int) (0.5 * hoehe), laenge, hoehe);

      // Viereck zeichnen
      g.fillRect(viereck.x, viereck.y, viereck.width, 
                 viereck.height);
   }

   public boolean liegtPunktImElement(int x, int y) 
   {
      return viereck.contains(x,y);
   }
}
