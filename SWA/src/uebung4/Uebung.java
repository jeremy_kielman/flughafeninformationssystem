package uebung4;

public class Uebung {

	int MAX_ELEMENTS = 10;
	int topElement = -1;
	Integer[] elements = new Integer[MAX_ELEMENTS];
	
	// assert Ausdruck1 : Ausdruck2;
	// zB.: assert isEmpty(element) : "Das Element ist " + element; 
	// zB.: assert(element == 0 || element == 1 || element == 2);
	
	public void push(int element){
		assert(numberOfElements() < MAX_ELEMENTS && numberOfElements() >= 0);
		int numberOfElements = numberOfElements();
		elements[++topElement] = element;
		assert(numberOfElements + 1 == numberOfElements() && numberOfElements() < MAX_ELEMENTS && numberOfElements() >= 0);
	}
	
	public int pop(){
		assert(numberOfElements() < MAX_ELEMENTS && numberOfElements() >= 0);
		int numberOfElements = numberOfElements();
		elements[topElement] = null;
		--topElement;
		assert(numberOfElements - 1 == numberOfElements() && numberOfElements() < MAX_ELEMENTS && numberOfElements() >= 0);
		return  numberOfElements();
	}
	
	public boolean isEmpty(){
		assert(numberOfElements() < MAX_ELEMENTS && numberOfElements() >= 0);
		if(numberOfElements() <= 0){
			return true;
		}
		else{
			return false;
		}
	}
	
	public boolean isFull(){
		assert(numberOfElements() < MAX_ELEMENTS && numberOfElements() >= 0);
		if(numberOfElements() < MAX_ELEMENTS){
			return true;
		}
		else{
			return false;
		}
	}
	
	public int numberOfElements(){
		assert(topElement + 1 < MAX_ELEMENTS && topElement + 1 >= 0);
		return topElement + 1;
	}
	
	
}
