package uebung4;

public class Main {
	
	public static void main(String[] args) {
		Uebung uebung = new Uebung();
		uebung.push(10);
		uebung.push(20);
		uebung.push(30);
		uebung.push(40);
		uebung.push(50);
		
		while(!uebung.isEmpty()){
			int value = uebung.pop();
			System.out.println(value);
			System.out.println(" ");
		}
	}
}
